# -*- coding: utf-8 -*-
"""
Created on Tue Jun 17 14:45:17 2014

@author: D.V.S. Phanindra
"""

import wx

import Dialog_AdvancedSettings_GUI

class Dialog_AdvancedSettings(Dialog_AdvancedSettings_GUI.Dialog_AdvancedSettings):
    """ Sub Class the GUI created with wxFormBuilder and extend the functionality """
    def __init__(self,parent,timeout=0,blocksize=1000):
        Dialog_AdvancedSettings_GUI.Dialog_AdvancedSettings.__init__(self,parent)
        
        self.timeout = timeout
        self.blocksize = blocksize
        self.OK_Clicked = False
        
        self.textCtrl_timeOut.SetValue(str(self.timeout))
        self.textCtrl_BlockSize.SetValue(str(self.blocksize))
        
    def button_ResetDefault_Click(self,event):
        self.textCtrl_timeOut.SetValue('0')
        self.textCtrl_BlockSize.SetValue('1000')
        
    def button_OK_Click(self,event):
        self.timeout = int(self.textCtrl_timeOut.GetValue())
        self.blocksize = int(self.textCtrl_BlockSize.GetValue())
        self.OK_Clicked = True
        
        self.Close()
        
    def onQuit(self,event):
        self.Close()

###############################################################################
                
if __name__ == "__main__":
    app = wx.App(False)
    
    GUI = Dialog_AdvancedSettings(None,5,200)
    
    GUI.Show(True)
    
    app.MainLoop()
        
        


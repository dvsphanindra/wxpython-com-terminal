from distutils.core import setup
import py2exe
 
includes = []
excludes = ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter']
packages = []
dll_excludes = ['libgdk-win32-2.0-0.dll', 'libgobject-2.0-0.dll', 'tcl84.dll',
                'tk84.dll']

icon_folder = "." # where to keep the icons
icon_files = ["Alecive-Flatwoken-Apps-Terminal-Pc-104.ico"] # List of icons
exe_ico = "Alecive-Flatwoken-Apps-Terminal-Pc-104.ico"  # icon for the executable file
script = "COM_terminal.py" # Main script file

dist_dir = "distribute"  # Directory name for the final exe file
 
setup(options = {"py2exe": {"compressed": 2, "optimize": 2,"includes": includes,"excludes": excludes,"packages": packages,
					"dll_excludes": dll_excludes,"bundle_files": 3,"dist_dir": dist_dir,"xref": False,
					"skip_archive": False,"ascii": False,"custom_boot_script": '',}},
					windows=[
									{
										"script": script,
										"icon_resources": [(50, exe_ico),(51, exe_ico),(52, exe_ico)]
									}
								],
					data_files=[(icon_folder,icon_files)])

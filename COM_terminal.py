# -*- coding: utf-8 -*-
# Author: D.V.S. Phanindra
# Created on 19 Feb, 2014 at 11:30 PM
# 

import wx
import wx.html

import sys

import COM_terminal_GUI
import Dialog_AdvancedSettings

import serial

import threading

import time

from pydispatch import dispatcher

import os

import platform

import glob
								
class Serial_Read (threading.Thread):
	def __init__(self,serial_object,blockSize = 1000):
		threading.Thread.__init__(self)
		dispatcher.connect(self.stop_thread, signal="StopSerial",sender=dispatcher.Any)
		self.serial_object = serial_object
		self.blockSize = blockSize
		self.readExitFlag = 0

#------------------------------------------------------------------------------
	def run(self):
		print "Monitoring COM port"
		self.read_SerialPort()
		print "Exiting COM port read"

#------------------------------------------------------------------------------
	def read_SerialPort(self):
		""" Reads the the serial port and 'dispatches' the data to the subscribers """
		print "Read Thread Started: Listening..."
		while(not(self.readExitFlag)):
			try:
				data = self.serial_object.read(self.blockSize)
			except Exception:
				pass
		
			if data:
				#dispatcher.send("SerialReceive", message=data)
				wx.CallAfter(dispatcher.send, "SerialReceive", '',data) # Thread Safe method to transfer data
				data = ""
			time.sleep(0.01)

#------------------------------------------------------------------------------
	def stop_thread(self):
		""" Thread is stopped by this method """
		self.readExitFlag = 1
		
###############################################################################		

class COM_terminal(COM_terminal_GUI.frame_Main):
	""" Sub Class the GUI created with wxFormBuilder and extend the functionality """
	
	def __init__(self, parent):
		COM_terminal_GUI.frame_Main.__init__(self,parent)
		
		self.OS = platform.system()
		
		
		
		self.portChoice = {}
		#self.portChoice = {0:"COM1",1:"COM2",2:"COM3",3:"COM4",4:"COM5",5:"COM6",6:"COM7",7:"COM8"}
		self.scanForComPorts()
		self.baudrateChoice = {0:9600,1:19200,2:57600,3:115200,4:230400,5:250000,6:312500,7:357142,8:460800,9:500000,10:576000,11:921600}
		self.parityChoice = {0:serial.PARITY_NONE,1:serial.PARITY_EVEN,2:serial.PARITY_ODD,3:serial.PARITY_MARK,4:serial.PARITY_SPACE}
		self.databitChoice = {0:serial.SIXBITS,1:serial.SEVENBITS,2:serial.EIGHTBITS}
		self.stopbitChoice = {0:serial.STOPBITS_ONE,1:serial.STOPBITS_ONE_POINT_FIVE,2:serial.STOPBITS_TWO}
		self.textCtrl_BaudRate.SetValue(str(self.baudrateChoice[self.choice_Baudrate.GetSelection()]))
		
		self.dirname = u'C:\\SerialLogger\\'
		self.logfilename = self.dirname + "logfile.txt"
		self.logfile = None
		self.SetStatusText("Not Logging",3)
		self.textCtrl_LogFile.SetValue(self.logfilename)
		
		self.baudrate = 9600
		self.timeOut = 0
		self.blockSize = 1000
		
		self.button_Clear.Disable()
		self.button_clearReceiveBox.Disable()
		self.button_SendData.Disable()
		
		if self.portChoice == {}:
			self.toggleBtn_COMOpen.Disable()
		else:
			self.toggleBtn_COMOpen.Enable()
		
		self.serial_port = None
		
		self.SetIcon(wx.Icon('Alecive-Flatwoken-Apps-Terminal-Pc-104.ico', wx.BITMAP_TYPE_ICO, 32, 32))
		
		
		dispatcher.connect(self.UART_ReceiveMessage, signal="SerialReceive",sender=dispatcher.Any)
		
#------------------------------------------------------------------------------
	def UART_ReceiveMessage(self, message):
		"""Updates the Receive textbox"""
		self.textCtrlRcv.AppendText(message)
		self.SetStatusText("Buffer Status: " + str(self.serial_port.inWaiting()),4)
		if self.toggleBtn_StartLogging.GetValue():
			self.logfile.write(message)

#------------------------------------------------------------------------------
	def frameMain_Close(self,event):
		
		print "Closing Serial Port Terminal..."
		try:
			if not self.serial_port == None:
				if self.serial_port.isOpen():
					dispatcher.send("StopSerial")
					self.serial_port.close()
		except Exception, e:
			print "Error closing serial port terminal"
			print "%s"%str(e)
		
		print "Closing Log File..."
		try:
			if not self.logfile == None:
				self.logfile.close()
		except Exception, e:
			print "Error while closing log file"
			print "%s"%str(e)
		
		print "Quitting... Bye"
		self.Destroy()		

#------------------------------------------------------------------------------
	def buttonClick_Clear(self,event):
		self.textCtrlSend.SetValue("")

#------------------------------------------------------------------------------
	def button_CopytoClipboard_Click(self,event):
		do = wx.TextDataObject()
		do.SetText(self.textCtrlRcv.GetValue())
		try:
			wx.TheClipboard.Open()
			wx.TheClipboard.SetData(do)
			wx.TheClipboard.Close()
		except Exception, e:
			print "Error Occurred while Copying to Clipboard:"
			print "%s" % str(e)
			wx.MessageBox(str(e), 'Error', wx.OK | wx.ICON_ERROR)
		else:
			wx.MessageBox("Copied to Clipboard",'Sucess',wx.OK|wx.ICON_INFORMATION)

#------------------------------------------------------------------------------
	def saveCurrentSettings_Click(self,event):
		f = open('settings.ini','w')
		f.write('Databits='+str(self.radioBox_Databits.GetSelection())+'\n')
		f.write('Parity='+str(self.radioBox_Parity.GetSelection())+'\n')
		f.write('Stopbits='+str(self.radioBox_Stopbits.GetSelection())+'\n')
		f.write('Baudrate='+str(self.baudrate)+'\n')
		f.write('Baudratechoice='+str(self.choice_Baudrate.GetSelection())+'\n')
		f.write('Blocksize='+str(self.blockSize)+'\n')
		f.write('Timeout='+str(self.timeOut)+'\n')
		f.close()

#------------------------------------------------------------------------------
	def loadSavedSettings_Click(self,event):
		# Load previous settings from configuration file
		try:
			f = open('settings.ini','r')
		except:
			print "Configuration file not found"
		else:
			i=0
			value = []
			for line in f:
				line = line.rstrip()
				(setting,v) = line.split('=')
				value.append(v)
				#print str(setting)+" : "+str(value[i])
				i += 1
			if i == 7:
				#print value
				self.radioBox_Databits.SetSelection(int(value[0]))
				self.radioBox_Parity.SetSelection(int(value[1]))
				self.radioBox_Stopbits.SetSelection(int(value[2]))
				self.baudrate = int(value[3])
				self.textCtrl_BaudRate.SetValue(value[3])
				self.choice_Baudrate.SetSelection(int(value[4]))				
				self.blockSize = int(value[5])
				self.timeOut = int(value[6])
			f.close()
			
#------------------------------------------------------------------------------
	def resetToDefaultSettings_Click(self,event):
		self.radioBox_Databits.SetSelection(1)
		self.radioBox_Parity.SetSelection(0)
		self.radioBox_Stopbits.SetSelection(0)
		self.baudrate = 9600
		self.textCtrl_BaudRate.SetValue("9600")
		self.choice_Baudrate.SetSelection(0)				
		self.blockSize = 1000
		self.timeOut = 0
			
#------------------------------------------------------------------------------ 

	def menu_AdvancedSettings_Click(self,event):
		dlg = Dialog_AdvancedSettings.Dialog_AdvancedSettings(self,self.timeOut,self.blockSize)
		dlg.ShowModal()
		
		if dlg.OK_Clicked:
			self.blockSize = dlg.blocksize
			self.timeOut = dlg.timeout
		
		dlg.Destroy()

	def buttonClick_SendData(self,event):
		string = str(self.textCtrlSend.GetValue())
		print string
		self.serial_port.write(string)

	def toggleBtn_COMOpen_Click(self,event):
		
		if not self.toggleBtn_COMOpen.GetValue():
			try:
				self.serial_port.close()
			except Exception, e:
				print "Error Occurred while Closing the requested serial port:"
				print "%s" % str(e)
				wx.MessageBox(str(e), 'Error', wx.OK | wx.ICON_ERROR)
			else:
				self.SetStatusText("Closed " + self.port,0)
				self.SetStatusText("",1)
				self.SetStatusText("",2)
				self.toggleBtn_COMOpen.SetLabel("Open COM Port")
				
				dispatcher.send("StopSerial")
				
				self.button_Clear.Disable()
				self.button_clearReceiveBox.Disable()
				self.button_SendData.Disable()
				
				self.choice_Baudrate.Enable()
				self.choice_COM.Enable()
				self.radioBox_Databits.Enable()
				self.radioBox_Parity.Enable()
				self.radioBox_Stopbits.Enable()
				
				self.advancedSettings.Enable(True)
				self.menu_scanForComPorts.Enable(True)
				self.menu_loadSavedSettings.Enable(True)
				self.menu_resetToDefaultSettings.Enable(True)
		else:
			
			self.port = self.portChoice[self.choice_COM.GetSelection()] 
			if self.choice_Baudrate.GetSelection() == 12:
				self.baudrate = self.textCtrl_BaudRate.GetValue()
			else:
				self.baudrate=self.baudrateChoice[self.choice_Baudrate.GetSelection()]
				
			self.dataBits = self.databitChoice[self.radioBox_Databits.GetSelection()]
			self.stopBits = self.stopbitChoice[self.radioBox_Stopbits.GetSelection()]
			self.parityBits = self.parityChoice[self.radioBox_Parity.GetSelection()]
			
			print "Opening " + self.port + ": "+ str(self.baudrate)
			
			try:
				#self.serial_port = serial.Serial(self.port[self.choice_COM.GetSelection()],self.baudrate[self.choice_Baudrate.GetSelection()],parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.SEVENBITS,timeout=0)
				self.serial_port = serial.Serial(self.port,self.baudrate,parity=self.parityBits,stopbits=self.stopBits,bytesize=self.dataBits,timeout=self.timeOut)
			except Exception, e:
				print "Error Occurred while Opening the requested serial port:"
				print "%s" % str(e)
				wx.MessageBox(str(e), 'Error', wx.OK | wx.ICON_ERROR)
				self.toggleBtn_COMOpen.SetValue(True)
			else:
				print("Connected to: " + self.serial_port.portstr)
				self.SetStatusText('Connected to ' + self.port,0)
				self.SetStatusText(str(self.baudrate),1)
				self.SetStatusText(str(self.dataBits) + '-' + str(self.stopBits) + '-' + self.parityBits,2)# + ' | t = ' + str(self.timeOut) + ' | chunk =' + str(self.blockSize),2)
				if self.logfile != None:
					self.SetStatusText(self.dirname+self.logfilename,3)
				else:
					self.SetStatusText("Not Logging",3)
				self.toggleBtn_COMOpen.SetLabel("Close COM Port")
				
				self.button_Clear.Enable()
				self.button_clearReceiveBox.Enable()
				self.button_SendData.Enable()
				
				self.choice_COM.Disable()
				self.choice_Baudrate.Disable()
				self.radioBox_Databits.Disable()
				self.radioBox_Parity.Disable()
				self.radioBox_Stopbits.Disable()
				self.advancedSettings.Enable(False)
				self.menu_scanForComPorts.Enable(False)
				self.menu_loadSavedSettings.Enable(False)
				self.menu_resetToDefaultSettings.Enable(False)
				# Start a serial port reading thread
				self.read_thread = Serial_Read(self.serial_port,self.blockSize)
				self.read_thread.start()
	
	def button_clearReceiveBox_Click(self,event):
		""" Clears the Receive text box"""
		self.textCtrlRcv.SetValue("")
		
	def On_About_Select(self, event):
		dlg = AboutBox()
		dlg.ShowModal()
		dlg.Destroy()
		
	def menu_selectLogFileDir_Click(self,event):
		dialog = wx.DirDialog(None, "Choose a directory:",style=wx.DD_DEFAULT_STYLE | wx.DD_NEW_DIR_BUTTON)
		
		if dialog.ShowModal() == wx.ID_OK:
			self.dirname = dialog.GetPath()
			self.logfilename = self.dirname + u"\logfile.txt"
			self.textCtrl_LogFile.SetValue(self.logfilename)
		
		dialog.Destroy()
	
	def menu_scanForComPorts_Click(self,event):
		self.scanForComPorts()
		
	def scanForComPorts(self):	   
		self.choice_COM.Clear()
		self.portChoice = {}
		if self.OS == "Windows":
			key = 0
			for i in range(256):
				try:
					s = serial.Serial("COM"+str(i))
				except serial.SerialException:
					pass
				else:
					self.portChoice.update({key:s.portstr})
					self.choice_COM.Append(s.portstr)
					key = key + 1
					s.close()
		elif self.OS == "Linux":
			key = 0
			s = glob.glob('/dev/ttyUSB*')
			print s
			for i in range(s.__len__()-1,-1,-1):
				self.portChoice.update({key:s[i]})
				self.choice_COM.Append(s[i])
				key = key + 1
				
			s = glob.glob('/dev/ttyS[0-2]')
			for i in range(s.__len__()-1,-1,-1):
				self.portChoice.update({key:s[i]})
				self.choice_COM.Append(s[i])
				key = key + 1
				
		if self.portChoice == {}:
			wx.MessageBox('No COM ports found. Please check the connections and serial port driver.', 'Verify COM port', wx.OK | wx.ICON_EXCLAMATION)
			self.toggleBtn_COMOpen.Disable()
		else:
			self.choice_COM.SetSelection(0)
			self.toggleBtn_COMOpen.Enable()
			
		#print "Found ports:"
		#print self.portChoice
			
	def toggleBtn_StartLogging_Click(self,event):
		if self.toggleBtn_StartLogging.GetValue():
			try:
				if not os.path.exists(self.dirname):
					os.makedirs(self.dirname)
				self.logfile = open(self.logfilename,'w')
			except Exception, e:
				print "Error Occurred while Opening the requested file:"
				print "%s" % str(e)
				wx.MessageBox(str(e), 'Error', wx.OK | wx.ICON_ERROR)
				self.toggleBtn_StartLogging.SetValue(True)
			else:
				self.toggleBtn_StartLogging.SetLabel("Stop Logging")
				self.SetStatusText("Logging...",3)
				self.selectLogFileDir.Enable(False)
		else:
			self.logfile.close()
			self.SetStatusText("Not Logging",3)
			self.toggleBtn_StartLogging.SetLabel("Start Logging")
			self.selectLogFileDir.Enable(True)
		
	def choice_Baudrate_Click(self, event):
		"Executes when arbitrary Baud rate is selected"
		if self.choice_Baudrate.GetSelection() == 12:
			self.textCtrl_BaudRate.SetValue("")
			self.textCtrl_BaudRate.Enable()
			self.textCtrl_BaudRate.SetFocus()
		else:
			self.textCtrl_BaudRate.SetValue(str(self.baudrateChoice[self.choice_Baudrate.GetSelection()]))
			self.textCtrl_BaudRate.Disable()
				
###############################################################################
aboutText = """<p><font face="Trebuchet" color = 	#CD853F size = 14>Simple COM Port Terminal: Version 0.5</font></p> <p>Developed by <b>D.V.S. Phanindra</b>.</p>
<p>Currently running on <b>wxPython</b> %(wxpy)s, <b>PySerial</b> %(pyserial)s and <b>Python</b> %(python)s.</p></p>"""

###############################################################################

class HtmlWindow(wx.html.HtmlWindow):
	""" Opens an HTML window for the About box """
	def __init__(self, parent, id, size=(600,400)):
		wx.html.HtmlWindow.__init__(self,parent, id, size=size)
		if "gtk2" in wx.PlatformInfo:
			self.SetStandardFonts()
			
#------------------------------------------------------------------------------
			
	def OnLinkClicked(self, link):
		""" If the link in the About box is clicked, open the website """
		wx.LaunchDefaultBrowser(link.GetHref())

###############################################################################
class AboutBox(wx.Dialog):
	""" The window to display details About the project """
	
	def __init__(self):
		wx.Dialog.__init__(self, None, -1, "About",
			style=wx.DEFAULT_DIALOG_STYLE|wx.THICK_FRAME|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL)
		hwin = HtmlWindow(self, -1, size=(450,200))
		vers = {}
		vers["python"] = sys.version.split()[0]
		vers["wxpy"] = wx.VERSION_STRING
		vers["pyserial"] = serial.VERSION
		hwin.SetPage(aboutText % vers)
		
		irep = hwin.GetInternalRepresentation()
		hwin.SetSize((irep.GetWidth()+25, irep.GetHeight()+10))
		self.SetClientSize(hwin.GetSize())
		self.CentreOnParent(wx.BOTH)
		self.SetFocus()
###############################################################################
				
if __name__ == "__main__":
	app = wx.App(False)
	
	GUI = COM_terminal(None)
	
	GUI.Show(True)
	
	app.MainLoop()


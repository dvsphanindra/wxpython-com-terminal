# COM Terminal Software

wxPython based COM terminal Software built around [pyserial](https://pypi.python.org/pypi/pyserial) module.

## Description

* This is a simple COM terminal software developed using Python. I developed it for myself. Felt that it will be useful for others as well. So I am uploading it here.
* Version 0.5

## How do I get set up?

This code has the following package dependencies:
* wxpython
* pyserial
* pydispatch

### via pip

```sudo pip install wxpython, pyserial, pydispatch```

### via [Anaconda](https://www.continuum.io/what-is-anaconda) distribution
If you had installed Anaconda, you can install the above dependencies using the `conda` command.
```
conda install wxpython
conda install pyserial
conda install pydispatch
```

## Contribution guidelines

I am uploading the software **AS IS**. Currently, there is no documentation available. I have not stuck to PEP8 guidelines as it is the standard for open-source Python projects.
You are free to contribute in whichever way you feel like such as, adding a feature, documentation, making the code compatible to PEP8 guidelines or anything else you can think of that will make the software useful to the community.

## Disclaimer

This software has been uploaded **AS IS**. Though I had used it on Linux and Windows, there is no guarantee that this software is bug-free. Use with caution.

## Credits
Terminal icon taken from [Alecive](https://github.com/alecive)

## License
![Alt text](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
This work is licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).


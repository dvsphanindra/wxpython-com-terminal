# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

ID_SCAN_FOR_COM_PORTS = 1000
ID_SELECT_LOG_FILE = 1001
ID_ADVANCED_SETTINGS = 1002
ID_SAVE_CURRENT_SETTINGS = 1003
ID_LOAD_SAVED_SETTINGS = 1004
ID_RESET_TO_DEFAULT_SETTINGS = 1005
ID_EXIT = 1006
ID_ABOUT_SIMPLE_COM_TERMINAL = 1007

###########################################################################
## Class frame_Main
###########################################################################

class frame_Main ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Simple COM Port Terminal", pos = wx.DefaultPosition, size = wx.Size( 649,600 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.menubar = wx.MenuBar( 0 )
		self.file = wx.Menu()
		self.menu_scanForComPorts = wx.MenuItem( self.file, ID_SCAN_FOR_COM_PORTS, u"Scan for COM ports", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.menu_scanForComPorts )
		
		self.file.AppendSeparator()
		
		self.selectLogFileDir = wx.MenuItem( self.file, ID_SELECT_LOG_FILE, u"Select log file Directory...", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.selectLogFileDir )
		
		self.advancedSettings = wx.MenuItem( self.file, ID_ADVANCED_SETTINGS, u"Advanced Settings...", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.advancedSettings )
		
		self.file.AppendSeparator()
		
		self.menu_saveCurrentSettings = wx.MenuItem( self.file, ID_SAVE_CURRENT_SETTINGS, u"Save Current Settings", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.menu_saveCurrentSettings )
		
		self.menu_loadSavedSettings = wx.MenuItem( self.file, ID_LOAD_SAVED_SETTINGS, u"Load Saved Settings", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.menu_loadSavedSettings )
		
		self.menu_resetToDefaultSettings = wx.MenuItem( self.file, ID_RESET_TO_DEFAULT_SETTINGS, u"Reset to Default Settings", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.menu_resetToDefaultSettings )
		
		self.file.AppendSeparator()
		
		self.exit = wx.MenuItem( self.file, ID_EXIT, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.file.AppendItem( self.exit )
		
		self.menubar.Append( self.file, u"File" ) 
		
		self.about = wx.Menu()
		self.aboutSimpleComTerminal = wx.MenuItem( self.about, ID_ABOUT_SIMPLE_COM_TERMINAL, u"About Simple COM terminal", wx.EmptyString, wx.ITEM_NORMAL )
		self.about.AppendItem( self.aboutSimpleComTerminal )
		
		self.menubar.Append( self.about, u"About" ) 
		
		self.SetMenuBar( self.menubar )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
		self.m_panel1.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_SCROLLBAR ) )
		
		bSizer10 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer5 = wx.BoxSizer( wx.VERTICAL )
		
		
		bSizer5.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.staticTextSelectCOM = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Select the COM Port:", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.staticTextSelectCOM.Wrap( -1 )
		self.staticTextSelectCOM.SetFont( wx.Font( 9, 74, 90, 92, False, "Arial" ) )
		
		bSizer3.Add( self.staticTextSelectCOM, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		choice_COMChoices = [ u"COM1", u"COM2", u"COM3", u"COM4", u"COM5", u"COM6", u"COM7", u"COM8" ]
		self.choice_COM = wx.Choice( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_COMChoices, 0 )
		self.choice_COM.SetSelection( 0 )
		bSizer3.Add( self.choice_COM, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		choice_BaudrateChoices = [ u"9600", u"19200", u"57600", u"115200", u"230400", u"250000", u"312500", u"357142", u"460800", u"500000", u"576000", u"921600", u"Arbitrary..." ]
		self.choice_Baudrate = wx.Choice( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_BaudrateChoices, 0 )
		self.choice_Baudrate.SetSelection( 0 )
		bSizer3.Add( self.choice_Baudrate, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.textCtrl_BaudRate = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_NO_VSCROLL )
		self.textCtrl_BaudRate.Enable( False )
		
		bSizer3.Add( self.textCtrl_BaudRate, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.toggleBtn_COMOpen = wx.ToggleButton( self.m_panel1, wx.ID_ANY, u"Open COM Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.toggleBtn_COMOpen.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer3.Add( self.toggleBtn_COMOpen, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		bSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		bSizer5.Add( bSizer3, 0, wx.EXPAND, 5 )
		
		self.m_staticText5 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Transmit Data:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		self.m_staticText5.SetFont( wx.Font( 9, 74, 90, 92, False, "Arial" ) )
		
		bSizer5.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.textCtrlSend = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
		bSizer5.Add( self.textCtrlSend, 2, wx.ALL|wx.EXPAND, 5 )
		
		bSizer2 = wx.BoxSizer( wx.HORIZONTAL )
		
		
		bSizer2.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.button_SendData = wx.Button( self.m_panel1, wx.ID_ANY, u"Send Data", wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		self.button_SendData.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer2.Add( self.button_SendData, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer2.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.button_Clear = wx.Button( self.m_panel1, wx.ID_ANY, u"Clear", wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		self.button_Clear.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer2.Add( self.button_Clear, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer2.AddSpacer( ( 0, 0), 8, wx.EXPAND, 5 )
		
		
		bSizer5.Add( bSizer2, 0, wx.EXPAND, 5 )
		
		self.staticText_RcvData = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Received Data:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_RcvData.Wrap( -1 )
		self.staticText_RcvData.SetFont( wx.Font( 9, 74, 90, 92, False, "Arial" ) )
		
		bSizer5.Add( self.staticText_RcvData, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.textCtrlRcv = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		bSizer5.Add( self.textCtrlRcv, 6, wx.ALL|wx.EXPAND, 5 )
		
		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )
		
		
		bSizer4.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.button_clearReceiveBox = wx.Button( self.m_panel1, wx.ID_ANY, u"Clear Received Data", wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		self.button_clearReceiveBox.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer4.Add( self.button_clearReceiveBox, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.button_CopytoClipboard = wx.Button( self.m_panel1, wx.ID_ANY, u"Copy to Clipboard", wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		self.button_CopytoClipboard.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer4.Add( self.button_CopytoClipboard, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer4.AddSpacer( ( 0, 0), 5, wx.EXPAND, 5 )
		
		
		bSizer5.Add( bSizer4, 0, wx.EXPAND, 5 )
		
		
		bSizer8.Add( bSizer5, 1, wx.EXPAND, 5 )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		
		bSizer6.AddSpacer( ( 0, 0), 1, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		radioBox_DatabitsChoices = [ u"6 bits", u"7 bits", u"8 bits" ]
		self.radioBox_Databits = wx.RadioBox( self.m_panel1, wx.ID_ANY, u"No of Data bits", wx.DefaultPosition, wx.DefaultSize, radioBox_DatabitsChoices, 1, wx.RA_SPECIFY_COLS )
		self.radioBox_Databits.SetSelection( 2 )
		bSizer6.Add( self.radioBox_Databits, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		radioBox_StopbitsChoices = [ u"1 bits", u"1.5 bits", u"2 bits" ]
		self.radioBox_Stopbits = wx.RadioBox( self.m_panel1, wx.ID_ANY, u"No of Stop bits", wx.DefaultPosition, wx.DefaultSize, radioBox_StopbitsChoices, 1, wx.RA_SPECIFY_COLS )
		self.radioBox_Stopbits.SetSelection( 0 )
		bSizer6.Add( self.radioBox_Stopbits, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		radioBox_ParityChoices = [ u"No Parity", u"Even Parity", u"Odd Parity", u"Mark", u"Space" ]
		self.radioBox_Parity = wx.RadioBox( self.m_panel1, wx.ID_ANY, u"Parity", wx.DefaultPosition, wx.DefaultSize, radioBox_ParityChoices, 1, wx.RA_SPECIFY_COLS )
		self.radioBox_Parity.SetSelection( 0 )
		bSizer6.Add( self.radioBox_Parity, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		bSizer6.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		
		bSizer8.Add( bSizer6, 0, wx.EXPAND, 5 )
		
		
		bSizer10.Add( bSizer8, 1, wx.EXPAND, 5 )
		
		bSizer7 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText3 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Log File name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )
		self.m_staticText3.SetFont( wx.Font( 9, 74, 90, 92, False, "Arial" ) )
		
		bSizer7.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.textCtrl_LogFile = wx.TextCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.textCtrl_LogFile.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_GRAYTEXT ) )
		self.textCtrl_LogFile.Enable( False )
		
		bSizer7.Add( self.textCtrl_LogFile, 1, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.toggleBtn_StartLogging = wx.ToggleButton( self.m_panel1, wx.ID_ANY, u"Start Logging", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.toggleBtn_StartLogging.SetFont( wx.Font( 10, 74, 90, 90, False, "Tahoma" ) )
		
		bSizer7.Add( self.toggleBtn_StartLogging, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer10.Add( bSizer7, 0, wx.EXPAND, 5 )
		
		
		self.m_panel1.SetSizer( bSizer10 )
		self.m_panel1.Layout()
		bSizer10.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 2 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		self.statusBar = self.CreateStatusBar( 5, wx.ST_SIZEGRIP|wx.SUNKEN_BORDER, wx.ID_ANY )
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.frameMain_Close )
		self.Bind( wx.EVT_MENU, self.menu_scanForComPorts_Click, id = self.menu_scanForComPorts.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_selectLogFileDir_Click, id = self.selectLogFileDir.GetId() )
		self.Bind( wx.EVT_MENU, self.menu_AdvancedSettings_Click, id = self.advancedSettings.GetId() )
		self.Bind( wx.EVT_MENU, self.saveCurrentSettings_Click, id = self.menu_saveCurrentSettings.GetId() )
		self.Bind( wx.EVT_MENU, self.loadSavedSettings_Click, id = self.menu_loadSavedSettings.GetId() )
		self.Bind( wx.EVT_MENU, self.resetToDefaultSettings_Click, id = self.menu_resetToDefaultSettings.GetId() )
		self.Bind( wx.EVT_MENU, self.frameMain_Close, id = self.exit.GetId() )
		self.Bind( wx.EVT_MENU, self.On_About_Select, id = self.aboutSimpleComTerminal.GetId() )
		self.choice_Baudrate.Bind( wx.EVT_CHOICE, self.choice_Baudrate_Click )
		self.toggleBtn_COMOpen.Bind( wx.EVT_TOGGLEBUTTON, self.toggleBtn_COMOpen_Click )
		self.button_SendData.Bind( wx.EVT_BUTTON, self.buttonClick_SendData )
		self.button_Clear.Bind( wx.EVT_BUTTON, self.buttonClick_Clear )
		self.button_clearReceiveBox.Bind( wx.EVT_BUTTON, self.button_clearReceiveBox_Click )
		self.button_CopytoClipboard.Bind( wx.EVT_BUTTON, self.button_CopytoClipboard_Click )
		self.toggleBtn_StartLogging.Bind( wx.EVT_TOGGLEBUTTON, self.toggleBtn_StartLogging_Click )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def frameMain_Close( self, event ):
		event.Skip()
	
	def menu_scanForComPorts_Click( self, event ):
		event.Skip()
	
	def menu_selectLogFileDir_Click( self, event ):
		event.Skip()
	
	def menu_AdvancedSettings_Click( self, event ):
		event.Skip()
	
	def saveCurrentSettings_Click( self, event ):
		event.Skip()
	
	def loadSavedSettings_Click( self, event ):
		event.Skip()
	
	def resetToDefaultSettings_Click( self, event ):
		event.Skip()
	
	
	def On_About_Select( self, event ):
		event.Skip()
	
	def choice_Baudrate_Click( self, event ):
		event.Skip()
	
	def toggleBtn_COMOpen_Click( self, event ):
		event.Skip()
	
	def buttonClick_SendData( self, event ):
		event.Skip()
	
	def buttonClick_Clear( self, event ):
		event.Skip()
	
	def button_clearReceiveBox_Click( self, event ):
		event.Skip()
	
	def button_CopytoClipboard_Click( self, event ):
		event.Skip()
	
	def toggleBtn_StartLogging_Click( self, event ):
		event.Skip()
	

